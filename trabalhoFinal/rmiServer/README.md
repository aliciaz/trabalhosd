# CONFIGURAÇÃO

- As properties do Servidor foram configuradas via arquivo .properties, caso deseje mudar algo, será necessário gerar um novo jar do projeto antes de executá-lo.

# GERANDO UM NOVO JAR

[NA PLATAFORMA LINUX OU MACOS]

- Via linha de Comando

1 - ir ate o path do projeto (rmiServer)
2 - entrar na pasta src 
    `$ cd src/`
3 - compilar o java 
    `$ javac -cp "mail-1.4.4.jar" br/edu/uerj/rmi/EmailService.java br/edu/uerj/rmi/server/Server.java`
4 - gerar o jar 
    `$ jar -cvfm NomeDoJar.jar META-INF/MANIFEST.MF mail-1.4.4.jar br/edu/uerj/rmi/EmailService.class br/edu/uerj/rmi/server/Server.class properties/info.properties br/edu/uerj/rmi/server/Server\$1.class`
5 - executar o jar
    `$ java -jar NomeDoJar.jar`


- Via MakeFile

1 - Ir até o path do projeto (rmiServer)
2 - executar o Makefile
    $ make -f Makefile


[NO WINDOWS E DEMAIS]

Gerar .jar através do eclipse

`Botão direito em cima do projeto -> Export-> Runnable JAR file`

`Launch Configuration: Server - rmiServer`

`Export Destination: Coloque aonde quer salvar o JAR`

`Library  handling: Package required libraries into generated JAR`

`Finish`

[Importante] Se você salvou num path diferente o JAR, entrar na pasta em que o JAR está salvo para executar o comando de execução


# EXECUÇÃO

- Via linha de comando

`$ java -jar NomeDoJar.jar`

- Via Eclipse

`Project -> Run As -> JavaApplication`

Exemplo de execução:

`$ java -jar ServerRMIJava.jar`

