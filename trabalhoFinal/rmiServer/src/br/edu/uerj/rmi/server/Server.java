package br.edu.uerj.rmi.server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.io.FileInputStream; 
import java.io.FileNotFoundException;
import java.io.IOException; 
import java.util.Properties; 

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import br.edu.uerj.rmi.EmailService;


public class Server implements EmailService{

	public static Properties getProp() throws IOException { 
        Properties props = new Properties();
        try {
        	FileInputStream file = new FileInputStream( "./properties/info.properties");
        	props.load(file);
        } catch( FileNotFoundException e2) {
        	FileInputStream file = new FileInputStream( "src/properties/info.properties");
        	props.load(file);
        }
        return props; 
	}

	@Override
	public Boolean enviaEmail(String msg, String destinatario){
		String smtpHost, smtpSSL, smtpSubject, smtpAuth;
		final String smtpMail, smtpPassword;
		int smtpPort;

		Properties props = new Properties();
		Properties prop = null;
		try {
			prop = getProp();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Método enviarEmail foi chamado.");
		
		smtpHost = prop.getProperty("prop.smtp.host");
	    smtpPort = Integer.parseInt(prop.getProperty("prop.smtp.port"));
	    smtpMail = prop.getProperty("prop.smtp.mail");
	    smtpPassword = prop.getProperty("prop.smtp.password");
	    smtpSubject = prop.getProperty("prop.smtp.subject");
	    smtpSSL = prop.getProperty("prop.smtp.ssl");
	    smtpAuth = prop.getProperty("prop.smtp.auth");
    
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.socketFactory.port", smtpPort);
        props.put("mail.smtp.socketFactory.class",smtpSSL);
        props.put("mail.smtp.port", smtpPort);
        props.put("mail.smtp.auth", smtpAuth);
        
        System.out.println("[host] "+smtpHost);
        System.out.println("[remetente] "+smtpMail);	    
		System.out.println("[msg] "+msg);
		System.out.println("[email destinatario] "+destinatario);
        
        Session session = Session.getDefaultInstance(props,
            new javax.mail.Authenticator() {
                 protected PasswordAuthentication getPasswordAuthentication() 
                 {
                       return new PasswordAuthentication(smtpMail, smtpPassword);
                 }
            });

        /** Ativa Debug para sessão */
        session.setDebug(false);

        try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(smtpMail)); //Remetente

			Address[] toUser = InternetAddress //Destinatário(s)
			         .parse(destinatario);  

			message.setRecipients(Message.RecipientType.TO, toUser);
			message.setSubject(smtpSubject);//Assunto
			message.setText(msg);
			/** Enviar a mensagem criada*/
			Transport.send(message);

			System.out.println("Mensagem enviada!!!");				

			return true;

         } catch (MessagingException e) {
			throw new RuntimeException(e);
        }
    }

	public static void main(String args[]) throws Exception {

		String bindName; 
	    int rmiPort;
	    System.out.println("---- Lendo rmiServer properties ----");
	    Properties prop = getProp();

	    rmiPort = Integer.parseInt(prop.getProperty("prop.rmi.port"));
	    bindName = prop.getProperty("prop.rmi.bindname");

	    System.out.println("[rmiPort] " + rmiPort); 
	    System.out.println("[bindName] " + bindName);

    	/* Instancia servico */
    	Server obj = new Server();
    	
    	/* Exporta o objeto remoto tornando-o disponivel para receber chamadas */
        EmailService service = (EmailService) UnicastRemoteObject.exportObject(obj , 0);

        /* Inicia o RMI Registry */
        Registry registry = LocateRegistry.createRegistry(rmiPort);
        System.out.println("Iniciando o RMI Registry...");
        
        /* Associa o servico a um nome e o registra no RMI Registry */
        registry.rebind(bindName , service);
        System.out.println("Registrando servico RMI no RMI Registry...");
        System.out.println("Servico de Email RMI registrado com bindName ["+bindName+"] na porta ["+rmiPort+"].");
    }
}