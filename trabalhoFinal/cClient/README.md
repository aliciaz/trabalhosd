# COMPILAÇÃO

`$ make -f Makefile`

# EXECUÇÃO

`$ ./client [host] [porta]`

** A porta deverá ser a mesma onde o servidor Java foi startado **

Exemplo de execução:

` ./client localhost 8090`
