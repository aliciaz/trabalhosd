#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, msg_socket, email_mensagem, email_destinatario;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[256];
    if (argc < 3) {
       fprintf(stderr,"Parametros necessarios para executacao: %s [hostname] [porta]\n", argv[0]);
       exit(0);
    }
    
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    
    if (sockfd < 0) 
        error("[ERRO] abrindo o socket");
    server = gethostbyname(argv[1]);
    
    if (server == NULL) {
        fprintf(stderr,"[ERRO] host não existe\n");
        exit(0);
    }
    
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("[ERRO] de conexão");
    
    printf("Entre com a mensagem: ");
    bzero(buffer,256);
    fgets(buffer,255,stdin);
    email_mensagem = write(sockfd,buffer,strlen(buffer));
    
    printf("Entre com o email do destinatário: ");
    bzero(buffer,256);
    fgets(buffer,255,stdin);
    email_destinatario = write(sockfd,buffer,strlen(buffer));
    
    if (email_mensagem < 0 || email_destinatario < 0)
         error("[ERRO] ao escrever para o socket");
    
    bzero(buffer,256);

    msg_socket = read(sockfd,buffer,255);
    
    if (msg_socket < 0) 
         error("[ERRO] na leitura do socket");
    printf("%s\n",buffer);
    
    close(sockfd);
    
    return 0;
}
