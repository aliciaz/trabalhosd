package br.edu.uerj.socket.client;

import java.io.*;
import java.net.*;

public class Cliente{
	public static void main(String[] args) throws IOException{
		Socket socket = null;
		PrintWriter out = null;
		BufferedReader in = null;

		// args[0] = hostname
		// args[1] = port number

		if (args.length < 2){
			System.out.println("Parametros necessarios para executacao: [hostname] [porta]\n");
			System.exit(1);
		}

		int port = Integer.parseInt(args[1]);
		String hostname = args[0];

		try{
			/* Abre socket para se comunicar com o servidor */ 
			
			/* [TO DO] AQUI O SERVIDOR � O RMI */
			socket = new Socket(hostname, port);
			
			/* Cria fluxo de sa�da para enviar mensagem para o servidor */
			out = new PrintWriter(socket.getOutputStream(), true);
			
			/* Cria fluxo de entrada para receber mensagem do servidor */
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		}catch (UnknownHostException e){
			System.err.println("Don't know about host: " + hostname + " \n");
			System.exit(1);
		}catch (IOException e){
			System.err.println("Couldn't get I/O for the connection to:"+ hostname + " .\n");
			System.exit(1);
		}

		System.out.println("Client is connected to server at " + hostname+ ", at port " + port + ".\n");

		/* Buffer para ler do teclado */
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		String userInput;

		/* Usuario digita a mensagem */
		
		/*[TO DO] Pega a mensagem que recebeu do m�dulo de servidor */
		System.out.println("Digite sua mensagem na linha abaixo e encerre com <enter> para enviar para o servidor:");
		if ((userInput = stdIn.readLine()) != null) {
			/* Envia mensagem para o servidor */
			out.println(userInput); 
		}
		
		/* Imprime mensagem recebida do servidor */
		System.out.println("Servidor: " + in.readLine());

		out.close();
		in.close();
		stdIn.close();
		socket.close();
	}
}