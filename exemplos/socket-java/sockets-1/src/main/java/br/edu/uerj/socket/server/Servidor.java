package br.edu.uerj.socket.server;

import java.net.*;
import java.io.*;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import br.edu.uerj.rmi.HelloService;

import java.io.FileInputStream; 
import java.io.IOException; 
import java.util.Properties; 


public class Servidor {

    public static Properties getProp() throws IOException { 
        Properties props = new Properties(); 
        FileInputStream file = new FileInputStream( "./properties/info.properties"); 
        props.load(file); 
        return props; 
    }

    public static void main(String[] args) throws Exception {
 
        String rmiHost, bindName; 
        int serverPort, rmiPort;
        System.out.println("---- Lendo server properties ----");
        Properties prop = getProp();

        serverPort =  Integer.parseInt(prop.getProperty("prop.server.port"));
        rmiHost = prop.getProperty("prop.rmi.host");
        rmiPort = Integer.parseInt(prop.getProperty("prop.rmi.port"));
        bindName = prop.getProperty("prop.rmi.bindname");

        System.out.println("[serverPort] " + serverPort); 
        System.out.println("[rmiHost] " + rmiHost); 
        System.out.println("[rmiPort] " + rmiPort); 
        System.out.println("[bindName] " + bindName);

        ServerSocket serverSocket = null;
        try{
            /* Cria Socket servidor em determinada porta */
            serverSocket = new ServerSocket(serverPort);
        }catch (IOException e){
            System.err.println("Nao foi possivel acessar a porta " + serverPort+".");
            System.exit(1);
        }
 
        /* Representa o cliente que ira se acoplar ao servidor */
        Socket clientSocket = null;
        try{
            System.out.println("Server esperando conexao na porta ["+serverPort+"]...");
            clientSocket = serverSocket.accept();
        }catch (IOException e){
            System.err.println("Erro de conexao.");
            System.exit(1);
        }
        
        System.out.println("Server aguardando cliente...");

        /* Fluxo de saida que permite enviar mensagem ao cliente */
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        
        /* Fluxo de entrada que permite receber mensagens do cliente */
        BufferedReader in = new BufferedReader(
            new InputStreamReader(clientSocket.getInputStream()));
        
        String inputLineMsg, inputLineDestinatario, outputLine;
                
        /* Le e imprime mensagem recebida do cliente */
        inputLineMsg = in.readLine();
        inputLineDestinatario = in.readLine();

        System.out.println("Mensagem a ser enviada: " + inputLineMsg);
        System.out.println("Email do destinatario: "+ inputLineDestinatario);
   
        String msg = inputLineMsg;
        String destinatario = inputLineDestinatario;
        
        System.out.println("Invocando metodo remoto...");
        
        /* Obtem referencia para objeto do RMI Registry que esta executando*/
        Registry registry = LocateRegistry.getRegistry(rmiHost, rmiPort);
        System.out.println("[host] "+rmiHost);
        System.out.println("[porta] "+ rmiPort);
        
        /* Busca no RMI Registry o servico desejado */
        HelloService email = (HelloService) registry.lookup(bindName);
        System.out.println("[bindName] "+ bindName);
        
        //  Executa o metodo do servico desejado 
        Boolean respostaServico = email.sayHello(msg, destinatario);
        
        System.out.println("Email enviado com sucesso? ["+ respostaServico +"]");

        /* Envia mensagem para o cliente */
        System.out.println("Retornando mensagem para o cliente socket...");

        if (respostaServico){
            outputLine = "Mensagem enviada com [sucesso]";
        }else{
            outputLine = "Mensagem enviada com [falha]";
        }

        out.println(outputLine);
        
        System.out.println("Encerrando conexão com o cliente socket...");
        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
    }
}

