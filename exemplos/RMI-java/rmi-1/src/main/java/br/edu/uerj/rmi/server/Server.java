package br.edu.uerj.rmi.server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import br.edu.uerj.rmi.HelloService;

import java.io.FileInputStream; 
import java.io.IOException; 
import java.util.Properties; 

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class Server implements HelloService{

	public static Properties getProp() throws IOException { 
        Properties props = new Properties(); 
        FileInputStream file = new FileInputStream( "./properties/info.properties"); 
        props.load(file); 
        return props; 
    }

	@Override
	public Boolean sayHello(String msg, String destinatario){
		String smtpHost, smtpMail, smtpPassword, smtpSubject;
		int smtpPort;
		
		Properties props = new Properties();
		Properties prop = getProp();

		smtpHost = prop.getProperty("prop.smtp.host");
	    smtpPort = Integer.parseInt(prop.getProperty("prop.smtp.port"));
	    smtpMail = prop.getProperty("prop.smtp.mail");
	    smtpPassword = prop.getProperty("prop.smtp.password");
	    smtpSubject = prop.getProperty("prop.smtp.subject");

        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.port", smtpPort);
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props,
            new javax.mail.Authenticator() {
                 protected PasswordAuthentication getPasswordAuthentication() 
                 {
                       return new PasswordAuthentication(smtpMail, smtpPassword);
                 }
            });

        /** Ativa Debug para sessão */
        session.setDebug(true);

        try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(smtpMail)); //Remetente

			Address[] toUser = InternetAddress //Destinatário(s)
			         .parse(destinatario);  

			message.setRecipients(Message.RecipientType.TO, toUser);
			message.setSubject(smtpSubject);//Assunto
			message.setText(msg);
			/**Método para enviar a mensagem criada*/
			Transport.send(message);

			System.out.println("Feito!!!");				
			System.out.println("Enviando msg...["+msg+"]");
			System.out.println("Para...["+destinatario+"]");
			return true;

         } catch (MessagingException e) {
			throw new RuntimeException(e);
			return false;
        }
    }

	public static void main(String args[]) throws Exception{
		try{
			String bindName; 
		    int rmiPort;
		    System.out.println("---- Lendo server properties ----");
		    Properties prop = getProp();

		    rmiPort = Integer.parseInt(prop.getProperty("prop.rmi.port"));
		    bindName = prop.getProperty("prop.rmi.bindname");

		    System.out.println("rmiPort = " + rmiPort); 
		    System.out.println("bindName = " + bindName);

	    	/* Instancia servico */
	    	Server obj = new Server();
	    	
	    	/* Exporta o objeto remoto tornando-o disponivel para receber chamadas */
	        HelloService service = (HelloService) UnicastRemoteObject.exportObject(obj , 0);

	        /* Inicia o RMI Registry */
	        Registry registry = LocateRegistry.createRegistry(rmiPort);
	        
	        /* Associa o servico a um nome e o registra no RMI Registry */
	        registry.rebind(bindName , service);

	        System.out.println("Server ready bindName:["+bindName+"] - Port:["+rmiPort+"]");
		}catch(ArrayIndexOutOfBoundsException exc){
			System.out.println("Usage:\n\tjava br.edu.uerj.rmi.server.Server port bindName \"message\"\n\n");
		}
    }
}
