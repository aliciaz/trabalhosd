package br.edu.uerj.rmi.client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import br.edu.uerj.rmi.EmailService;

public class Client{
    /**
     * Executa o programa principal
     * @param args
     * @throws Exception
     */
    public static void main(String args[]) throws Exception{
    	try{
	    	String host	= args[0];
	    	int port = Integer.parseInt(args[1]);
	    	String bindName	= args[2];
	    	String name	= args[3];
	    	
	    	System.out.println("Invocando metodo remoto em:\n"
	    			+ "host....:["+host+"]\n"
	    			+ "port....:["+port+"]\n"
	    			+ "bindName:["+bindName+"]\n"
	    			+ "name....:["+name+"]");
	    	
	    	/* Obtem refer�ncia para objeto do RMI Registry que est� executando*/
	        Registry registry = LocateRegistry.getRegistry(host, port);
	        
	        /* Busca no RMI Registry o servi�o desejado */
	        EmailService email = (EmailService) registry.lookup(bindName);
	
	        /* Executa o m�todo do servi�o desejado */
	        String message = email.enviaEmail(name); //M�todo � executado remotamente e o resultado � retornado
	        
	        System.out.println("Mensagem recebida do servidor:["+ message + "]");  
    	}catch(ArrayIndexOutOfBoundsException exc){
    		System.out.println("Usage:\n\tjava br.edu.uerj.rmi.client.Client host port bindName name\n\n");
    	}
    }
}
