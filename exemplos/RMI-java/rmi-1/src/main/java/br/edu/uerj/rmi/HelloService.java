package br.edu.uerj.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface HelloService extends Remote {
    /**
    * Metodo que recebe uma mensagem e o endereco de e-mail de um destinatario.
    * Ele tenta enviar a mensagem para o destinatario
    * @param mensagem
    * @param email
    * @return Se enviar com sucesso retorna true senao retorna false
    * @throws RemoteException Excecao lancada caso ocorra problemas na comunicacao RMI
    */
	Boolean sayHello(String mensagem, String destinatario) throws RemoteException;
}

